package controllers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/rappopo/pusdata-api/lib"
	"gitlab.com/rappopo/pusdata-api/models"
)

func FindAisCurrents(c *gin.Context) {
	db := c.MustGet("dbAis").(*gorm.DB)

	var aisCurrents []models.AisCurrent
	var count uint
	offset, limit, order := lib.CrudFindParams(c)

	if order == "id" {
		order = "updated_at desc"
	}

	age, _ := strconv.Atoi(c.DefaultQuery("age", "1"))
	latest := time.Now().AddDate(0, 0, -age).UTC().Format(time.RFC3339)
	db = db.Where("updated_at > ?", latest).Order(order).Offset(offset).Limit(limit)

	err := db.Find(&aisCurrents).Offset(0).Limit(-1).Count(&count).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": aisCurrents, "total": count})
}

func FindAisCurrent(c *gin.Context) {
	db := c.MustGet("dbAis").(*gorm.DB)

	var aisCurrent models.AisCurrent
	if err := db.Where("mmsi = ?", c.Param("id")).First(&aisCurrent).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": aisCurrent})
}
