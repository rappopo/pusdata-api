package controllers

import (
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/rappopo/pusdata-api/lib"
	"gitlab.com/rappopo/pusdata-api/models"
)

type AuthForm struct {
	Username string `form:"username" binding:"required"`
	Password string `form:"password" binding:"required"`
}

func CreateToken(c *gin.Context) {
	db := c.MustGet("dbMain").(*gorm.DB)

	var input AuthForm
	if err := c.ShouldBind(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	var user models.User
	if err := db.Where("username = ?", input.Username).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid/unknown user"})
		return
	}

	err := lib.BCryptVerify(user.Password, input.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Password mismatch"})
		return
	}

	claims := lib.JwtClaims{
		user.ID,
		user.Token,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + 86400,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(lib.JwtKey)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": ss})
}
