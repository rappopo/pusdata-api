package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/rappopo/pusdata-api/lib"
	"gitlab.com/rappopo/pusdata-api/models"
)

type CreateUserForm struct {
	Username string `form:"username" binding:"required"`
	Password string `form:"password" binding:"required"`
	Token    string `form:"token"`
	Name     string `form:"name" binding:"required"`
	Email    string `form:"email" binding:"required"`
}

type UpdateUserForm struct {
	Password string `form:"password"`
	Token    string `form:"token"`
	Name     string `form:"name"`
	Email    string `form:"email"`
}

func FindUsers(c *gin.Context) {
	db := c.MustGet("dbMain").(*gorm.DB)

	var users []models.User
	var count uint
	offset, limit, order := lib.CrudFindParams(c)

	db = db.Offset(offset).Limit(limit)
	err := db.Order(order).Find(&users).Offset(0).Limit(-1).Count(&count).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": users, "total": count})
}

func FindUser(c *gin.Context) {
	db := c.MustGet("dbMain").(*gorm.DB)

	var user models.User
	if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": user})
}

func CreateUser(c *gin.Context) {
	db := c.MustGet("dbMain").(*gorm.DB)

	var input CreateUserForm
	if err := c.ShouldBind(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	hashedPasswd, _ := lib.BCryptHash(input.Password)
	input.Token = lib.MD5Hash(string(hashedPasswd))

	user := models.User{Username: input.Username, Password: string(hashedPasswd), Name: input.Name, Email: input.Email, Token: input.Token}
	err := db.Create(&user).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": user})

}

func UpdateUser(c *gin.Context) {
	db := c.MustGet("dbMain").(*gorm.DB)

	var user models.User
	if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found"})
		return
	}

	var input UpdateUserForm
	if err := c.ShouldBind(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	if input.Password != "" {
		hashedPasswd, _ := lib.BCryptHash(input.Password)
		input.Password = string(hashedPasswd)
		input.Token = lib.MD5Hash(input.Password)
	}

	err := db.Model(&user).Updates(input).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": user})

}

func DeleteUser(c *gin.Context) {
	db := c.MustGet("dbMain").(*gorm.DB)

	var user models.User
	if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found"})
		return
	}

	err := db.Delete(&user).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true})

}
