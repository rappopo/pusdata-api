package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/rappopo/pusdata-api/lib"
	"gitlab.com/rappopo/pusdata-api/models"
)

func FindAisHistoricals(c *gin.Context) {
	db := c.MustGet("dbAis").(*gorm.DB)

	var aisHistoricals []models.AisHistorical
	var count uint
	offset, limit, order := lib.CrudFindParams(c)

	if order == "id" {
		order = "id desc"
	}

	db = db.Offset(offset).Limit(limit)
	err := db.Order(order).Find(&aisHistoricals).Offset(0).Limit(-1).Count(&count).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": aisHistoricals, "total": count})
}

func FindAisHistorical(c *gin.Context) {
	db := c.MustGet("dbAis").(*gorm.DB)

	var aisHistorical models.AisHistorical
	if err := db.Where("id = ?", c.Param("id")).First(&aisHistorical).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": aisHistorical})
}
