package lib

import (
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func CrudFindParams(c *gin.Context) (int, int, string) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	limit, _ := strconv.Atoi(c.DefaultQuery("limit", "25"))
	parts := strings.Split(c.DefaultQuery("sort", "id"), ",")
	var orders []string
	for _, el := range parts {
		els := strings.Split(el, ":")
		var item = els[0]
		if len(els) > 1 {
			item += " " + strings.ToLower(els[1])
		}
		orders = append(orders, item)
	}
	order := strings.Join(orders, ", ")
	offset := (page - 1) * limit

	return offset, limit, order
}
