package lib

import (
	jwt "github.com/dgrijalva/jwt-go"
)

var JwtKey = []byte("PusdataApi")

type JwtClaims struct {
	UID   uint   `json:"uid"`
	Token string `json:"token"`
	jwt.StandardClaims
}
