package lib

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/buger/jsonparser"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func ReadConfig() []byte {
	jsonFile, err := os.Open("config.json")
	if err != nil {
		panic("Can't open config.json")
	}
	defer jsonFile.Close()
	byteVal, _ := ioutil.ReadAll(jsonFile)
	return byteVal
}

var Config []byte

func SetupDb(name string) *gorm.DB {
	username, _ := jsonparser.GetString(Config, "db", name, "username")
	password, _ := jsonparser.GetString(Config, "db", name, "password")
	dbname, _ := jsonparser.GetString(Config, "db", name, "dbname")
	host, _ := jsonparser.GetString(Config, "db", name, "host")

	connStr := "host=%s user=%s password=%s dbname=%s sslmode=disable"
	connStr = fmt.Sprintf(connStr, host, username, password, dbname)

	db, err := gorm.Open("postgres", connStr)

	if err != nil {
		panic(err)
	}

	return db
}
