package middlewares

import (
	"net/http"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/rappopo/pusdata-api/lib"
	"gitlab.com/rappopo/pusdata-api/models"
)

var user models.User

func Auth(c *gin.Context) {
	db := c.MustGet("dbMain").(*gorm.DB)
	split := strings.Split(c.Request.Header.Get("Authorization"), " ")
	if split[0] == "JWT" {
		token, err := jwt.ParseWithClaims(split[1], &lib.JwtClaims{}, func(t *jwt.Token) (interface{}, error) {
			return []byte(lib.JwtKey), nil
		})
		if claims, ok := token.Claims.(*lib.JwtClaims); ok && token.Valid {
			if err := db.Where("id = ?", claims.UID).First(&user).Error; err != nil {
				c.JSON(http.StatusUnauthorized, gin.H{"success": false, "message": "User not found"})
				c.Abort()
				return
			}
			if user.Token != claims.Token {
				c.JSON(http.StatusUnauthorized, gin.H{"success": false, "message": "Invalid token"})
				c.Abort()
				return
			}
		} else {
			c.JSON(http.StatusUnauthorized, gin.H{"success": false, "message": err.Error()})
			c.Abort()
		}
	} else if split[0] == "Basic" {
		if err := db.Where("token = ?", split[1]).First(&user).Error; err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"success": false, "message": "User not found"})
			c.Abort()
			return
		}
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"success": false, "message": "Unknown authorization method"})
		c.Abort()
	}
}
