package main

import (
	"strconv"

	"github.com/buger/jsonparser"
	"github.com/gin-gonic/gin"
	"gitlab.com/rappopo/pusdata-api/controllers"
	"gitlab.com/rappopo/pusdata-api/lib"
	"gitlab.com/rappopo/pusdata-api/middlewares"
	"gitlab.com/rappopo/pusdata-api/models"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	lib.Config = lib.ReadConfig()
	dbMain := lib.SetupDb("main")
	dbMain.AutoMigrate(&models.User{})
	dbAis := lib.SetupDb("ais")
	dbAis.AutoMigrate(&models.AisCurrent{})
	dbAis.AutoMigrate(&models.AisHistorical{})

	r.Use(func(c *gin.Context) {
		c.Set("dbMain", dbMain)
		c.Set("dbAis", dbAis)
		c.Next()
	})

	r.POST("/token", controllers.CreateToken)

	r.GET("/user", middlewares.Auth, controllers.FindUsers)
	r.GET("/user/:id", middlewares.Auth, controllers.FindUser)
	r.POST("/user", middlewares.Auth, controllers.CreateUser)
	r.PUT("/user/:id", middlewares.Auth, controllers.UpdateUser)
	r.DELETE("/user/:id", middlewares.Auth, controllers.DeleteUser)

	r.GET("/ais/current", middlewares.Auth, controllers.FindAisCurrents)
	r.GET("/ais/current/:id", middlewares.Auth, controllers.FindAisCurrent)

	r.GET("/ais/position/historical", middlewares.Auth, controllers.FindAisHistoricals)
	r.GET("/ais/position/historical/:id", middlewares.Auth, controllers.FindAisHistorical)

	port, _ := jsonparser.GetInt(lib.Config, "server", "port")
	r.Run(":" + strconv.FormatInt(port, 10))
}
