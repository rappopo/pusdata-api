package models

import (
	"time"
)

type AisCurrent struct {
	MMSI           string    `json:"mmsi" gorm:"primary_key"`
	UpdatedAt      time.Time `json:"updated_at"`
	IMO            string    `json:"imo"`
	CallSign       string    `json:"call_sign" gorm:"column:callsign"`
	ShipName       string    `json:"ship_name" gorm:"column:shipname"`
	ShipType       string    `json:"ship_type" gorm:"column:shiptype"`
	ToBow          int       `json:"to_bow"`
	ToStern        int       `json:"to_stern"`
	ToPortside     int       `json:"to_portside" gorm:"column:to_port"`
	ToStarboard    int       `json:"to_starboard"`
	Model          string    `json:"model"`
	Serial         string    `json:"serial"`
	Vendor         string    `json:"vendor" gorm:"column:vendorid"`
	Draught        float64   `json:"draught"`
	Destination    string    `json:"destination"`
	MothershipMMSI string    `json:"mothership_mmsi"`
	Type           int       `json:"type"`
	Status         int       `json:"status"`
	Speed          float64   `json:"speed"`
	Accuracy       float64   `json:"accuracy"`
	Lon            float64   `json:"lon"`
	Lat            float64   `json:"lat"`
	Course         float64   `json:"course"`
	Heading        int       `json:"heading"`
	Maneuver       int       `json:"maneuver"`
	Turn           float64   `json:"turn"`
}

type AisHistorical struct {
	ID        uint      `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	MMSI      string    `json:"mmsi" gorm:"primary_key"`
	UpdatedAt time.Time `json:"updated_at"`
	Type      int       `json:"type"`
	Status    int       `json:"status"`
	Speed     float64   `json:"speed"`
	Accuracy  bool      `json:"accuracy"`
	Lon       float64   `json:"lon"`
	Lat       float64   `json:"lat"`
	Course    float64   `json:"course"`
	Heading   int       `json:"heading"`
	Maneuver  int       `json:"maneuver"`
	Turn      float64   `json:"turn"`
}

func (AisCurrent) TableName() string {
	return "current"
}

func (AisHistorical) TableName() string {
	return "position_hist"
}
