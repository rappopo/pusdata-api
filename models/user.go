package models

import (
	"time"
)

type User struct {
	ID        uint      `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Username  string    `json:"username"`
	Token     string    `json:"-"`
	Password  string    `json:"-"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
}
