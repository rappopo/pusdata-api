module gitlab.com/rappopo/pusdata-api

go 1.14

require (
	github.com/appleboy/gin-jwt/v2 v2.6.3 // indirect
	github.com/buger/jsonparser v0.0.0-20191204142016-1a29609e0929
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.5.0
	github.com/jinzhu/gorm v1.9.12
	github.com/jondot/goweight v1.0.5 // indirect
	golang.org/x/crypto v0.0.0-20200311171314-f7b00557c8c4
)
